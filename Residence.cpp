//
//  Residence.cpp
//  Project2
//
//  Created by Rumeysa Bulut on 2.11.2017.
//  Copyright © 2017 Rumeysa Bulut. All rights reserved.
//

#include <iostream>
#include <sstream>  //for stringstream
#include <vector>
#include "Residence.hpp"
using namespace std;
Residence::Residence(){
    
}
Residence::Residence(string line){
    vector<string> ResidenceData;
    stringstream fullLine(line);
    string data;
    while(getline(fullLine, data, ',')){
        ResidenceData.push_back(data);
    }
    setPopulation(ResidenceData[0]);
    setMinAge(ResidenceData[1]);
    setMaxAge(ResidenceData[2]);
    setGender(ResidenceData[3]);
    setZipCode(ResidenceData[4]);
    setGeo_id(ResidenceData[5]);
}
void Residence::setPopulation(string population){
    this->population = stoi(population);
    
}
void Residence::setMinAge(string minAge){
    if(!minAge.empty())      //if minimum age is given
        this->minAge = stoi(minAge);
    else
        this->minAge = -1;      //if minimum age is not given
}
void Residence::setMaxAge(string maxAge){
    if(!maxAge.empty())      //if maximum age is given
        this->maxAge = stoi(maxAge);
    else
        this->maxAge = -1;      //if maximum age is not given
}
void Residence::setGender(string gender){
    this->gender = gender;
}
void Residence::setZipCode(string zipCode){
    this->zipCode = zipCode;
}
void Residence::setGeo_id(string geo_id){
    this->geo_id = geo_id;
}
int Residence::getPopulation(){
    return population;
}
int Residence::getMinAge(){
    return minAge;
}
int Residence::getMaxAge(){
    return maxAge;
}
string Residence::getGender(){
    return gender;
}
string Residence::getZipCode(){
    return zipCode;
}
string Residence::getGeo_id(){
    return geo_id;
}
