//
//  ResidenceSorter.hpp
//  Project2
//
//  Created by Rumeysa Bulut on 2.11.2017.
//  Copyright © 2017 Rumeysa Bulut. All rights reserved.
//

#ifndef ResidenceSorter_hpp
#define ResidenceSorter_hpp

#include <vector>
#include "Residence.hpp"
using namespace std;
class ResidenceSorter{
private:
    vector<Residence> willBeSorted;
public:
    ResidenceSorter(const char *);  //it will take N value, read N record from file and create the residences which will be sorted.
    
    void quickSort(long, long);
    long partition(long,long);
    void writeToFile();
};

#endif /* ResidenceSorter_hpp */
